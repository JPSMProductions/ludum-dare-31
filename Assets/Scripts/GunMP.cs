﻿using UnityEngine;
using System.Collections;

public class GunMP : MonoBehaviour 
{
	public Transform shootPoint;
	public bool left = false;
	
	private float delay = 0f;
	private UpgradeManager upgradeManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		delay -= Time.deltaTime;
		
		if( delay <= 0 && Input.GetKey(KeyCode.Space) )
		{
			delay = upgradeManager.upgradeShotSpeed / 4;
			GameObject bulletSrc = Resources.Load("MP Bullet") as GameObject;
			
			if( left )
			{
				if( upgradeManager.unlockedLeftDouble )
				{
					GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
					go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
					go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage / 5;
					
					Vector3 pos = shootPoint.position;
					pos.y += 0.5f;
					go = Instantiate(bulletSrc, pos, transform.rotation) as GameObject;
					go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
					go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage / 5;
				}
				else
				{
					GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
					go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
					go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage / 5;				
				}
			}
			else
			{
				if( upgradeManager.unlockedRightDouble )
				{
					GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
					go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
					go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage / 5;
					
					Vector3 pos = shootPoint.position;
					pos.y += 0.5f;
					go = Instantiate(bulletSrc, pos, transform.rotation) as GameObject;
					go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
					go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage / 5;
				}
				else
				{
					GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
					go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
					go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage / 5;				
				}
			}
		}
	}
}
