﻿using UnityEngine;
using System.Collections;

public class Gun : MonoBehaviour 
{
	public Transform shootPoint;
	
	private float delay = 0f;
	private UpgradeManager upgradeManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		delay -= Time.deltaTime;
		
		if( delay <= 0 && Input.GetKey(KeyCode.Space) )
		{
			delay = upgradeManager.upgradeShotSpeed;
			GameObject bulletSrc = Resources.Load("Bullet") as GameObject;
			GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
			go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed;
			go.GetComponent<Bullet>().damage = upgradeManager.upgradeBulletDamage;
		}
		
		if( GetComponentInParent<Health>().healthPoints <= 0 )
		{
			Application.LoadLevel(1);
		}
	}
	
	void OnTriggerEnter(Collider collider)
	{
		if( collider.tag == "Bullet" )
		{
			Bullet bullet = collider.gameObject.GetComponent<Bullet>();
			GetComponentInParent<Health>().Sub(bullet.damage);
			Destroy(bullet.gameObject);
		}
	}
}