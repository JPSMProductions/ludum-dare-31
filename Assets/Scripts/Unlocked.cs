﻿using UnityEngine;
using UnityEngine.UI;

public class Unlocked : MonoBehaviour 
{
	public UnlockLevel[] levels;
	
	private int level = 0;
	private int lastPLevel = 0;
	
	private MonsterManager monsterManager;
	private MoneyManager moneyManager;
	private UpgradeManager upgradeManager;
	private Text text;
	private bool canBuy = false;
	
	void Start()
	{
		monsterManager = GameObject.FindObjectOfType<MonsterManager>();
		moneyManager = GameObject.FindObjectOfType<MoneyManager>();
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
		text = GetComponentInChildren<Text>();
		
		text.text = "LOCKED!";
	}
	
	void Update()
	{
		if( levels.Length == 0 ) return;
		
		if( monsterManager.LEVEL != lastPLevel ) // Changed level
		{
			UpdateButton();
		}
	}
	
	void UpdateButton()
	{
		lastPLevel = monsterManager.LEVEL;
		if( level < levels.Length )
		{
			UnlockLevel lastLevel = (level == 0) ? null : levels[level - 1];
			bool buyed = (lastLevel == null) ? true : lastLevel.buyed;
			if( (monsterManager.LEVEL >= levels[level].neededLevel ) && buyed )
			{
				text.text = levels[level].uName + "\n" + levels[level].costs + "$";
				canBuy = true;
				level++;
			}
		}
	}
	
	public void Buy()
	{
		if( canBuy )
		{
			UnlockLevel unLvl = levels[level - 1];
			if( moneyManager.money >= unLvl.costs )
			{
				moneyManager.Sub(unLvl.costs);
				upgradeManager.DoUnlock(unLvl.sName);
				levels[level-1].buyed = true;
				
				text.text = "LOCKED!";
				Debug.Log("Unlocked: " + unLvl.uName + "("+unLvl.sName+")");
				
				UpdateButton();
			}
		}
	}
}

[System.Serializable]
public class UnlockLevel
{
	public string uName;
	public string sName;
	
	public int costs;
	public int neededLevel;
	
	public bool buyed = false;
}
