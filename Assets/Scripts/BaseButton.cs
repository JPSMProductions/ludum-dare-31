﻿using UnityEngine;
using UnityEngine.UI;

public class BaseButton : MonoBehaviour 
{
	private int costs = 0;
	private int level = 1;
	
	private MoneyManager moneyManager;
	private Health playerHealth;
	private Text text;
	
	void Start()
	{
		moneyManager = GameObject.FindObjectOfType<MoneyManager>();
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
		text = GetComponentInChildren<Text>();
	}
	
	void Update()
	{
		costs = Mathf.RoundToInt(666.6667f * (level*1.5f));
		text.text = "Base Upgrade ("+costs+"$)";
	}
	
	public void UpgradeBase()
	{
		if( moneyManager.money >= costs )
		{
			moneyManager.Sub(costs);
			level++;
			
			playerHealth.maxHealth *= 1.5f;
		}
	}
}
