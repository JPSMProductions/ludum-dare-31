﻿using UnityEngine;
using System.Collections;

public class GunRocket : MonoBehaviour 
{
	public Transform shootPoint;
	
	private float delay = 0f;
	private UpgradeManager upgradeManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		delay -= Time.deltaTime;
		
		if( delay <= 0 && Input.GetKey(KeyCode.Space) )
		{
			delay = upgradeManager.rocketShootTime;
			GameObject bulletSrc = Resources.Load("Rocket") as GameObject;
			GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
			
			Quaternion quad = new Quaternion();
			quad.eulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z - 90);
			go.transform.rotation = quad;
			
			go.GetComponent<Rocket>().speed = upgradeManager.rocketSpeed;
			go.GetComponent<Rocket>().damage = upgradeManager.rocketDamage;
		}
	}
}