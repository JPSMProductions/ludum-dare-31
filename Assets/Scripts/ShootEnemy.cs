﻿using UnityEngine;
using System.Collections;

public class ShootEnemy : Enemy
{
	// we have to spawn an bullet (mp-bullet) and shoot the player
	// The bullets have 75% of damage
	
	public Transform shootPoint;
	public float bulletSpeed = 5f;
	
	private float sDelay = 0;
	
	void Awake()
	{
		sDelay = attackDelay;
	}
	
	void LateUpdate()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		sDelay -= Time.deltaTime;
		
		if( sDelay <= 0 )
		{			
			GameObject bulletSrc = Resources.Load("MP Bullet") as GameObject;
			GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
			go.GetComponent<Bullet>().speed = -bulletSpeed;
			go.GetComponent<Bullet>().damage = (damage / 100f) * 75f;
			go.GetComponent<Bullet>().moveZ = true;
			
			sDelay = attackDelay;
		}
	}
}