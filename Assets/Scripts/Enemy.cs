﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour 
{
	public float speed = 5f;
	public float damage = 6f;
	public float attackDelay = 2f;
	
	public int money = 20;
	
	private Transform player;
	private Vector3 direction;
	private MoneyManager moneyManager;
	private MonsterManager monsterManager;
	private float delay = 0;
	
	void Start()
	{
		player = GameObject.FindGameObjectWithTag("Player").transform;
		moneyManager = GameObject.FindObjectOfType<MoneyManager>();
		monsterManager = GameObject.FindObjectOfType<MonsterManager>();
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		transform.LookAt(player);
		
		Quaternion quad = new Quaternion();
		quad.eulerAngles = new Vector3(0, transform.rotation.eulerAngles.y, 0);
		transform.rotation = quad;
		
		direction = new Vector3(0, 0, speed);
		delay -= Time.deltaTime;
	}
	
	void FixedUpdate()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		if( Vector3.Distance(player.position, transform.position) > 2.5f ) transform.Translate(direction * Time.fixedDeltaTime);
		else
		{
			if( delay <= 0 )
			{
				player.GetComponent<Health>().Sub(damage);
				delay = attackDelay;
			}
		}
	}
	
	void OnTriggerEnter(Collider collider)
	{
		if( collider.tag == "Bullet" || collider.tag == "F-Bullet" )
		{
			Bullet bullet = collider.gameObject.GetComponent<Bullet>();
			GetComponent<Health>().Sub(bullet.damage);
			Destroy(bullet.gameObject);
		}
		else if( collider.tag == "Rocket" || collider.tag == "F-Rocket" )
		{
			Rocket rocket = collider.gameObject.GetComponent<Rocket>();
			GetComponent<Health>().Sub(rocket.damage);
			Destroy(rocket.gameObject);
		}
	}
	
	public void Die()
	{
		monsterManager.KilledEnemy();
		moneyManager.Add(money); // TODO Random money (from x to y)
	}
}
