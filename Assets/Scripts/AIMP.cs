﻿using UnityEngine;
using System.Collections;

public class AIMP : MonoBehaviour 
{
	public Transform shootPoint;

	private float delay = 0f;
	private UpgradeManager upgradeManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		delay -= Time.deltaTime;
		
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		
		if( delay <= 0 && enemies.Length > 0 )
		{
			delay = (upgradeManager.unlockedAutoGunDelay) ? upgradeManager.upgradeShotSpeed : (upgradeManager.upgradeShotSpeed / 100f) * 125f;
			delay /= 4;
			
			GameObject bulletSrc = Resources.Load("MP Bullet") as GameObject;
			
			GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
			go.tag = "F-Bullet";
			go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed * 3;
			go.GetComponent<Bullet>().damage = (upgradeManager.unlockedAutoGunDamage) ? upgradeManager.upgradeBulletDamage : upgradeManager.upgradeBulletDamage / 2f;				
			go.GetComponent<Bullet>().damage /= 5;
		}
	}
}
