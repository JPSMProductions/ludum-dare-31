﻿using UnityEngine;
using System.Collections;

public class CanonEnemy : ShootEnemy
{
	private float cDelay = 0;
	
	void Awake()
	{
		cDelay = attackDelay;
	}
	
	void LateUpdate()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		cDelay -= Time.deltaTime;
		
		if( cDelay <= 0 )
		{			
			GameObject bulletSrc = Resources.Load("Bullet") as GameObject;
			GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
			go.GetComponent<Bullet>().speed = -bulletSpeed;
			go.GetComponent<Bullet>().damage = (damage / 100f) * 90f;
			go.GetComponent<Bullet>().moveZ = true;
			
			cDelay = attackDelay;
		}
	}
}
