﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour 
{
	public float healthPoints = 100;
	public float healthBarTopSpace = 30;
	
	public Vector2 healthBarSize = new Vector2(100, 20);

	public Texture2D healthBarBorderTexture;
	public Texture2D healthTexture;
	
	public bool destroyOnDead = false;
	public bool drawHealthBar = true;
	
	public MonoBehaviour onDieComp;
	public MonoBehaviour onHitComp;
	public MonoBehaviour onHealComp;
	
	internal float maxHealth;
	
	void Start()
	{
		maxHealth = healthPoints;
	}
	
	public void Sub(float val)
	{
		healthPoints -= val;
		
		if( onHitComp != null ) onHitComp.SendMessage("Hit");
		
		if( healthPoints <= 0f )
		{
			if( onDieComp != null) onDieComp.SendMessage("Die");
			if( destroyOnDead ) Destroy(gameObject);
		}
	}
	
	public void Add(float val)
	{
		healthPoints += val;
		
		if( onHealComp != null ) onHealComp.SendMessage("Hit");
	}
	
	public void Set(float val)
	{
		
		healthPoints = val;
		if( healthPoints <= 0f )
		{
			if( onDieComp != null) onDieComp.SendMessage("Die");
			if( destroyOnDead ) Destroy(gameObject);
		}
	}
	
	void OnGUI()
	{
		if( !drawHealthBar ) return;
	
		Vector3 pos3D = Camera.main.WorldToScreenPoint(transform.position);
		pos3D.z = 0;
		pos3D.y += healthBarTopSpace;
		pos3D.x -= healthBarSize.x / 2;
		
		GUI.DrawTexture(new Rect(pos3D.x, Screen.height - pos3D.y, healthBarSize.x, healthBarSize.y), healthBarBorderTexture);
		
		// max health (100%)
		float G = maxHealth;
		float W = healthPoints;
		
		float P = (W / G) * 100f;
		
		float width = ((healthBarSize.x - 2) / 100f) * P;
		GUI.DrawTexture(new Rect(pos3D.x + 1, Screen.height - pos3D.y + 1, width, healthBarSize.y - 2), healthTexture);
	}
}
