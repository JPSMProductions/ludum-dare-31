﻿using UnityEngine;
using UnityEngine.UI;

public class Upgrade : MonoBehaviour
{
	public string uName;
	public string sName;
	
	public float mainCosts = 100f;
	public float costsMultiplier = 1.5f;
	public float valueMultiplier = 1.5f;
	
	private int level;
	
	private static MoneyManager moneyManager;
	private static UpgradeManager upgradeManager;
	
	void Start()
	{
		GetComponentInChildren<Text>().text = uName + "\n" + mainCosts + "$";
		
		if( moneyManager == null ) moneyManager = GameObject.FindObjectOfType<MoneyManager>();
		if( upgradeManager == null ) upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	public void DoUpgrade()
	{
		int costs = Mathf.RoundToInt(mainCosts * (Mathf.Pow(costsMultiplier, level)));
		if( moneyManager.money >= costs )
		{
			moneyManager.Sub(costs);
			upgradeManager.DoUpgrade(valueMultiplier, sName);
			
			level++;
			int newCosts = Mathf.RoundToInt(mainCosts * (Mathf.Pow(costsMultiplier, level)));
			GetComponentInChildren<Text>().text = uName + "\n" + newCosts + "$";
		}
	}
}