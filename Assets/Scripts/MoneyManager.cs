﻿using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : MonoBehaviour 
{
	public int money = 0;
	public Text moneyText;
	
	void Update()
	{
		moneyText.text = "$ " + money;
	}
	
	public void Add(int val)
	{
		money += val;
	}
	
	public void Sub(int val)
	{
		money -= val;
	}
	
	public void Set(int val)
	{
		money = val;
	}
}
