﻿using UnityEngine;
using UnityEngine.UI;

public class HealButton : MonoBehaviour 
{
	private int costs = 0;
	
	private MonsterManager monsterManager;
	private MoneyManager moneyManager;
	private Health playerHealth;
	
	void Start()
	{
		monsterManager = GameObject.FindObjectOfType<MonsterManager>();
		moneyManager = GameObject.FindObjectOfType<MoneyManager>();
		playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
	}
	
	void Update()
	{
		float health = playerHealth.healthPoints;
		float maxCosts = 111f * (monsterManager.LEVEL * 1.2f);
		costs = Mathf.RoundToInt(maxCosts - ((maxCosts / playerHealth.maxHealth) * health));
		
		GetComponentInChildren<Text>().text = "Heal ("+costs+"$)";
	}
	
	public void Heal()
	{
		if( moneyManager.money >= costs )
		{
			moneyManager.Sub(costs);
			playerHealth.Set(playerHealth.maxHealth);
		}
	}
}
