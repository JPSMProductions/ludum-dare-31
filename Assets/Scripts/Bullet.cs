﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
	public float speed;
	public float damage;
	
	public bool moveZ = false;
	
	private Vector3 velocity;
	private float lifeTime = 4.2f;
	
	void Update()
	{
		if( !moveZ ) velocity = new Vector3(-speed * Time.deltaTime, 0, 0);
		else velocity = new Vector3(0, 0, -speed * Time.deltaTime);
		
		lifeTime -= Time.deltaTime;
		if( lifeTime <= 0 ) Destroy(gameObject);
	}
	
	void FixedUpdate()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		transform.Translate(velocity);
	}
	
	void OnCollisionEnter(Collision collision)
	{
		if( collision.gameObject.tag == "Bullet" || collision.gameObject.tag == "F-Bullet" )
		{
			Bullet bullet = collision.gameObject.GetComponent<Bullet>();
			if( bullet.damage > damage ) Destroy(gameObject);
		}
		else if( collision.gameObject.tag == "Rocket" || collision.gameObject.tag == "F-Rocket" ) Destroy(gameObject);
	}
}
