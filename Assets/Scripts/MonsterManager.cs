﻿using UnityEngine;
using System.Collections.Generic;

public class MonsterManager : MonoBehaviour 
{
	public int LEVEL { get { return level; } }
	
	public float spawnTick = 1f;
	
	public int spawnsPerTick = 1;
	public int needToLevelUp = 10;
	
	private static List<Vector3> spawns;
	private float delay = 0;
	
	private GameObject enemy1Src;
	private GameObject enemy2Src;
	private GameObject enemy3Src;
	private GameObject enemy4Src;
	private GameObject[] possibleEnemies;
	
	private int killedEnemies = 0;
	
	private static int level = 1;
	
	void Start()
	{
		if( spawns == null ) 
		{
			spawns = new List<Vector3>();
			
			GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("Monster Spawner");
			foreach(GameObject go in spawnPoints ) spawns.Add(go.transform.position);
		}
		
		enemy1Src = Resources.Load("Enemy 01") as GameObject; // TODO Pooling system
		enemy2Src = Resources.Load("Enemy 02") as GameObject;
		enemy3Src = Resources.Load("Enemy 03") as GameObject;
		enemy4Src = Resources.Load("Enemy 04") as GameObject;
		
		possibleEnemies = new GameObject[] {enemy1Src};
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		delay -= Time.deltaTime;
		
		if( delay <= 0 )
		{
			delay = spawnTick;
			for(int i = 0; i < Random.Range(1, spawnsPerTick + 1); i++ )
			{
				GameObject enemy = possibleEnemies[Random.Range(0, possibleEnemies.Length)];
				Instantiate(enemy, spawns[Random.Range(0, spawns.Count)], Quaternion.identity);
			}
		}
	}
	
	public void KilledEnemy()
	{
		killedEnemies++;
		
		if( killedEnemies >= needToLevelUp)
		{
			level++;
			needToLevelUp = Mathf.RoundToInt(needToLevelUp * 2.1f);
			Debug.Log("Reached level " + level + ". Need " + needToLevelUp + " dead Enemies to level up!");
			
			switch(level)
			{
				case 2:
					possibleEnemies = new GameObject[] {enemy1Src, enemy1Src, enemy1Src, enemy1Src, enemy1Src,
														enemy2Src};
				break;
				
				case 3:
					possibleEnemies = new GameObject[] {enemy1Src, enemy1Src, enemy1Src, enemy2Src, enemy2Src,
														enemy2Src};
					spawnTick -= 0.2f;
				break;
				
				case 4:
					possibleEnemies = new GameObject[] {enemy2Src, enemy2Src, enemy2Src, enemy2Src, enemy2Src,
														enemy3Src, enemy2Src, enemy2Src, enemy2Src};
					spawnTick -= 0.1f;
				break;
				
				case 5:
					possibleEnemies = new GameObject[] {enemy2Src, enemy2Src, enemy2Src, enemy3Src, enemy3Src,
														enemy3Src};
				break;
				
				case 6:
					possibleEnemies = new GameObject[] {enemy2Src, enemy3Src, enemy3Src, enemy3Src, enemy3Src,
														enemy4Src};
					spawnsPerTick++;
				break;
				
				case 7:
					possibleEnemies = new GameObject[] {enemy3Src, enemy3Src, enemy3Src, enemy3Src, enemy4Src,
														enemy4Src};
					spawnTick -= 0.05f;
				break;
				
				case 8:
					possibleEnemies = new GameObject[] {enemy3Src, enemy3Src, enemy3Src, enemy4Src, enemy4Src,
														enemy4Src};
					spawnsPerTick++;
				break;
			}
		}
	}
}
