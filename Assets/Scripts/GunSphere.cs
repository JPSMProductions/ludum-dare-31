﻿using UnityEngine;
using System.Collections;

public class GunSphere : MonoBehaviour 
{
	public Transform upgradeMenu;
	public Transform unlockedMenu;
	public Transform healButton;
	public Transform baseButton;

	private UpgradeManager upgradeManager;
	private MonsterManager monsterManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
		monsterManager = GameObject.FindObjectOfType<MonsterManager>();
	}

	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen )
		{
			upgradeMenu.gameObject.SetActive(true);
			unlockedMenu.gameObject.SetActive(true);
			healButton.gameObject.SetActive(true);
			
			if( monsterManager.LEVEL >= 7 ) baseButton.gameObject.SetActive(true);
			else baseButton.gameObject.SetActive(false);
			
			return;
		}
		upgradeMenu.gameObject.SetActive(false);
		unlockedMenu.gameObject.SetActive(false);
		healButton.gameObject.SetActive(false);
		baseButton.gameObject.SetActive(false);
		
		float velocity = 0;
		if( Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow) ) velocity -= 1;
		if( Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) ) velocity += 1;
		
		velocity *= upgradeManager.upgradeRotationSpeed * Time.deltaTime;
		transform.Rotate(0, 0, velocity);
	}
}
