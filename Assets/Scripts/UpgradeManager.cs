﻿using UnityEngine;
using System.Collections;

public class UpgradeManager : MonoBehaviour 
{
	public static bool upgradeMenuOpen = false;
	
	public Transform cameraPositionNormal, cameraPositionUpgrade;
	public Transform leftMP, rightMP, rocketLauncher, laserGun, autoGun;
	public Transform autoGunMPLeft, autoGunMPRight, autoGunRocketLauncher;
	public Transform autoGun2, autoGun2MPLeft, autoGun2MPRight, autoGun2RocketLauncher;
	public Transform autoGun3, autoGun3MPLeft, autoGun3MPRight, autoGun3RocketLauncher;
	public Transform autoGun4, autoGun4MPLeft, autoGun4MPRight, autoGun4RocketLauncher;
	
	public float smoothFactor = 0.3f;
	
	public float upgradeRotationSpeed = 100f;
	public float upgradeShotSpeed = 2f;
	public float upgradeBulletSpeed = 6f;
	public float upgradeBulletDamage = 10f;
	public float rocketShootTime = 10f;
	public float rocketSpeed = 8f;
	public float rocketDamage = 20f;
	public float rocketRotationSmooth = 0.05f;
	public float laserDamage = 10f;
	public float laserEnergy = 5f;
	
	public bool unlockedLeftMP = false;
	public bool unlockedRightMP = false;
	public bool	unlockedRocketLauncher = false;
	public bool unlockedRocketTargetSeeking = false;
	public bool unlockedLaserGun = false;
	public bool unlockedLeftDouble = false;
	public bool unlockedRightDouble = false;

	public bool unlockedAutoGun = false;
	public bool unlockedAutoGunDamage = false;
	public bool unlockedAutoGunDelay = false;
	public bool unlockedAutoGunLeft = false;
	public bool unlockedAutoGunRight = false;
	public bool unlockedAutoGunRockets = false;
	
	public bool unlockedAutoGun2 = false;
	public bool unlockedAutoGun2Damage = false;
	public bool unlockedAutoGun2Delay = false;
	public bool unlockedAutoGun2Left = false;
	public bool unlockedAutoGun2Right = false;
	public bool unlockedAutoGun2Rockets = false;
	
	public bool unlockedAutoGun3 = false;
	public bool unlockedAutoGun3Damage = false;
	public bool unlockedAutoGun3Delay = false;
	public bool unlockedAutoGun3Left = false;
	public bool unlockedAutoGun3Right = false;
	public bool unlockedAutoGun3Rockets = false;
	
	public bool unlockedAutoGun4 = false;
	public bool unlockedAutoGun4Damage = false;
	public bool unlockedAutoGun4Delay = false;
	public bool unlockedAutoGun4Left = false;
	public bool unlockedAutoGun4Right = false;
	public bool unlockedAutoGun4Rockets = false;
	
	public void ToggleUpgradeMenu()
	{
		if( upgradeMenuOpen )
		{
			upgradeMenuOpen = false;
		}
		else
		{
			upgradeMenuOpen = true;
		}
	}
	
	void Update()
	{
		if( upgradeMenuOpen )
		{
			if( Vector3.Distance(transform.position, cameraPositionUpgrade.position) > 1f ) 
			Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, cameraPositionUpgrade.position, smoothFactor);
			
			if( Camera.main.transform.rotation != cameraPositionUpgrade.rotation )
				Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, cameraPositionUpgrade.rotation, smoothFactor);
			
		}
		else
		{
			if( Vector3.Distance(transform.position, cameraPositionNormal.position) > 1f )
				Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, cameraPositionNormal.position, smoothFactor);
				
				
			if( Camera.main.transform.rotation != cameraPositionNormal.rotation )
				Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, cameraPositionNormal.rotation, smoothFactor);	
		}
	}
	
	public void DoUpgrade(float multiplier, string sName)
	{
		switch(sName)
		{
			case "rotation_speed":
				upgradeRotationSpeed *= multiplier;
			break;
			
			case "shot_speed":
				upgradeShotSpeed -= multiplier;
			break;
			
			case "bullet_speed":
				upgradeBulletSpeed *= multiplier;
			break;
			
			case "bullet_damage":
				upgradeBulletDamage *= multiplier;
			break;
		}
	}
	
	public void DoUnlock(string sName)
	{
		switch(sName)
		{
			case "left_mp":
				unlockedLeftMP = true;
				leftMP.gameObject.SetActive(true);
			break;
			
			case "right_mp":
				unlockedRightMP = true;
				rightMP.gameObject.SetActive(true);
			break;
			
			case "rocket_launcher":
				unlockedRocketLauncher = true;
				rocketLauncher.gameObject.SetActive(true);
			break;
			
			case "rocket_target":
				unlockedRocketTargetSeeking = true;
			break;
			
			case "laser_gun":
				unlockedLaserGun = true;
				laserGun.gameObject.SetActive(true);
			break;
			
			case "laser_strength":
				laserDamage *= 2;
			break;
			
			case "left_double":
				unlockedLeftDouble = true;
			break;
			
			case "right_double":
				unlockedRightDouble = true;
			break;
			
			case "auto_gun":
				unlockedAutoGun = true;
				autoGun.gameObject.SetActive(true);
			break;
			
			case "auto_gun_dmg":
				unlockedAutoGunDamage = true;
			break;
			
			case "auto_gun_delay":
				unlockedAutoGunDelay = true;
			break;
			
			case "auto_gun_left":
				unlockedAutoGunLeft = true;
				autoGunMPLeft.gameObject.SetActive(true);
			break;
			
			case "auto_gun_right":
				unlockedAutoGunRight = true;
				autoGunMPRight.gameObject.SetActive(true);
			break;
			
			case "auto_gun_rocket":
				unlockedAutoGunRockets = true;
				autoGunRocketLauncher.gameObject.SetActive(true);
			break;
			
			case "auto_gun_3":
				unlockedAutoGun3 = true;
				autoGun3.gameObject.SetActive(true);
				break;
				
			case "auto_gun_3_dmg":
				unlockedAutoGun3Damage = true;
				break;
				
			case "auto_gun_3_delay":
				unlockedAutoGun3Delay = true;
				break;
				
			case "auto_gun_3_left":
				unlockedAutoGun3Left = true;
				autoGun3MPLeft.gameObject.SetActive(true);
				break;
				
			case "auto_gun_3_right":
				unlockedAutoGun3Right = true;
				autoGun3MPRight.gameObject.SetActive(true);
				break;
				
			case "auto_gun_3_rocket":
				unlockedAutoGun3Rockets = true;
				autoGun3RocketLauncher.gameObject.SetActive(true);
				break;
				
			case "auto_gun_4":
				unlockedAutoGun4 = true;
				autoGun4.gameObject.SetActive(true);
				break;
				
			case "auto_gun_4_dmg":
				unlockedAutoGun4Damage = true;
				break;
				
			case "auto_gun_4_delay":
				unlockedAutoGun4Delay = true;
				break;
				
			case "auto_gun_4_left":
				unlockedAutoGun4Left = true;
				autoGun4MPLeft.gameObject.SetActive(true);
				break;
				
			case "auto_gun_4_right":
				unlockedAutoGun4Right = true;
				autoGun4MPRight.gameObject.SetActive(true);
				break;
				
			case "auto_gun_4_rocket":
				unlockedAutoGun4Rockets = true;
				autoGun4RocketLauncher.gameObject.SetActive(true);
				break;
		}
	}
}
