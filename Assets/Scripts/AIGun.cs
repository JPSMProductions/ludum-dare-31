﻿using UnityEngine;
using System.Collections;

public class AIGun : MonoBehaviour 
{
	public Transform shootPoint;
	
	private float delay = 0f;
	private UpgradeManager upgradeManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		float distance = 999f;
		GameObject aEnemy = null;
		
		foreach(GameObject go in enemies)
		{
			float dist = Vector3.Distance(go.transform.position, transform.position);
			if( dist < distance )
			{
				distance = dist;
				aEnemy = go;
			}
		}
		
		if( aEnemy != null )
		{
			Quaternion quad = new Quaternion();
			quad = Quaternion.LookRotation(aEnemy.transform.position - transform.position);
			quad.eulerAngles = new Vector3(-90, 0, quad.eulerAngles.y + 90 + 5);
			
			transform.rotation = quad;
		}
		
		delay -= Time.deltaTime;
		
		if( delay <= 0 && aEnemy != null )
		{
			delay = (upgradeManager.unlockedAutoGunDelay) ? upgradeManager.upgradeShotSpeed : (upgradeManager.upgradeShotSpeed / 100f) * 125f;
			GameObject bulletSrc = Resources.Load("Bullet") as GameObject;
			GameObject go = Instantiate(bulletSrc, shootPoint.position, transform.rotation) as GameObject;
			go.tag = "F-Bullet";
			go.GetComponent<Bullet>().speed = upgradeManager.upgradeBulletSpeed;
			go.GetComponent<Bullet>().damage = (upgradeManager.unlockedAutoGunDamage) ? upgradeManager.upgradeBulletDamage : upgradeManager.upgradeBulletDamage / 2f;
		}
	}
}
