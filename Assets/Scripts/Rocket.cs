﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour 
{
	public float speed;
	public float damage;
	
	private Vector3 velocity;
	private float lifeTime = 20f;
	
	private static UpgradeManager upgradeManager;
	
	void Start()
	{
		if( upgradeManager == null ) upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
	}
	
	void Update()
	{
		if( upgradeManager.unlockedRocketTargetSeeking )
		{
			GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
			float distance = 999f;
			GameObject aEnemy = null;
			
			foreach(GameObject go in enemies)
			{
				float dist = Vector3.Distance(go.transform.position, transform.position);
				if( dist < distance )
				{
					distance = dist;
					aEnemy = go;
				}
			}
			
			if( aEnemy != null )
			{
				GameObject tmpTrans = new GameObject("TEMP Transform");
				Quaternion q = new Quaternion();
				q.eulerAngles = new Vector3(270, 180, 0);
				tmpTrans.transform.rotation = q;
				tmpTrans.transform.position = transform.position;
				tmpTrans.transform.LookAt(aEnemy.transform);
				
				Quaternion quad = new Quaternion();
				quad.eulerAngles = new Vector3(270, tmpTrans.transform.eulerAngles.y, 0);
				
				transform.rotation = Quaternion.Lerp(transform.rotation, quad, upgradeManager.rocketRotationSmooth);
				Destroy(tmpTrans);
			}
		}
		
		velocity = new Vector3(0, -speed * Time.deltaTime, 0);
		
		lifeTime -= Time.deltaTime;
		if( lifeTime <= 0 ) Destroy(gameObject);
	}
	
	void FixedUpdate()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
		transform.Translate(velocity);
	}
}
