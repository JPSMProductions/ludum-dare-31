﻿using UnityEngine;
using System.Collections;

public class GunLaser : MonoBehaviour 
{
	private float delay	 = 0f;
	private UpgradeManager upgradeManager;
	
	void Start()
	{
		upgradeManager = GameObject.FindObjectOfType<UpgradeManager>();
		delay = upgradeManager.laserEnergy;
	}
	
	void Update()
	{
		if( UpgradeManager.upgradeMenuOpen ) return;
	
		if( Input.GetKey(KeyCode.Space) )
		{
			if( delay > 0 )
			{
				LineRenderer line = GetComponent<LineRenderer>();
				
				line.enabled = true;
				line.SetPosition(0, Vector3.zero);
				line.SetPosition(1, new Vector3(-600f, 0, 0));
				
				Ray ray = new Ray(transform.position, transform.TransformDirection(-1f, 0, 0));
	
				delay -= Time.deltaTime;
	
				foreach( RaycastHit hit in Physics.RaycastAll(ray, 600f) )
				{
					if( hit.collider.tag != "Enemy" ) continue;
					Health health = hit.collider.GetComponent<Health>();
					health.Sub(upgradeManager.laserDamage * Time.deltaTime);
				}
			} else GetComponent<LineRenderer>().enabled = false;
		}
		else
		{
			GetComponent<LineRenderer>().enabled = false;
		
			delay += Time.deltaTime;
			delay = Mathf.Clamp(delay, 0f, upgradeManager.laserEnergy);
		}
	}
}
